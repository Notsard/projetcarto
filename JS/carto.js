let request2G = 'https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=couverture-2g-3g-4g-en-france-par-operateur-juillet-2015&q=73000&rows=1000&facet=code_postal&facet=code_departement&facet=operateur&facet=type_couverture&facet=reseau&facet=metropole&refine.reseau=2G&refine.operateur=Bouygues+Telecom&refine.code_postal=73000';
let request4G = 'https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=couverture-2g-3g-4g-en-france-par-operateur-juillet-2015&facet=code_postal&facet=code_departement&facet=operateur&facet=type_couverture&facet=reseau&facet=metropole&refine.reseau=4G&refine.operateur=Bouygues+Telecom&refine.code_departement=73';
let data2G = null;
let data4G = null;
let tableauMarker4G = [];
let tableauMarker2G = [];
let state = null;
let mymap = L.map('mapid').setView([45.564601, 5.917781], 14);
L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {

    attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
    minZoom: 3,
    maxZoom: 50,
}).addTo(mymap);



var greenIcon = new L.Icon({
    iconUrl: './icons/marker-icon-2x-green.png',
    shadowUrl: './icons/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
var orangeIcon = new L.Icon({
    iconUrl: './icons/marker-icon-2x-orange.png',
    shadowUrl: './icons/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
let geoLocalisation = undefined;

init();

function init() {
    mymap.locate({
        setView: true,
        maxZoom: 16
    });

    loadData(request2G, create2GMarkers);
    loadData(request4G, create4GMarkers);


}

function loadData(request, callback) {
    fetch(request)
        .then(function (response) {
            return response.json();

        })
        .then(function (data) {
            callback(data.records);
        })
}


function create2GMarkers(data) {
    for (i = 0; i < data.length; i++) {
        let res = data[i].fields.coordonnees;
        let marker2G = L.marker(res).bindPopup("ce point est a tel coordonnees " + res + "").addTo(mymap);
        tableauMarker2G[i] = marker2G
    }

}

function remove2GMarkers() {

    for (i = 0; i < tableauMarker2G.length; i++) {
        let marker2G = tableauMarker2G[i];
        mymap.removeLayer(marker2G);
    }


}
/**
 * create the 4G marker on the mymap 
 */
function create4GMarkers(data) {
    for (i = 0; i < data.length; i++) {
        let res2 = data[i].fields.coordonnees;
        let marker = new L.marker(res2, {
                icon: greenIcon
            })
            .bindPopup("ce point est a tel coordonnees " + res2 + "").addTo(mymap);
        tableauMarker4G[i] = marker;
    }

}

/**
 * remove 4G marker on the mymap
 */
function remove4GMarkers() {
    for (i = 0; i < tableauMarker4G.length; i++) {
        let marker = tableauMarker4G[i]
        mymap.removeLayer(marker);
    }
}


function onClickRadioHandler(radio) {
    state = radio.value;
    markerDisplay();
}

function onLocationFound(e) {
    let radius = e.accuracy / 2;

    L.marker(e.latlng, {
            icon: orangeIcon
        }).addTo(mymap)
        .bindPopup("Vous êtes à " + radius + " mettres de ce points").openPopup();

    L.circle(e.latlng, radius).addTo(mymap);
    geoLocalisation = e.latlng;
}

mymap.on('locationfound', onLocationFound);

function onLocationError(e) {
    alert(e.message);
}

mymap.on('locationerror', onLocationError);

function markerDisplay() {
    remove2GMarkers();
    remove4GMarkers();
    if (state == '4G') {
        loadData(request4G,create4GMarkers);
        remove2GMarkers();
    } else {
        loadData(request2G,create2GMarkers);
        remove4GMarkers();
    }

}